import pytest


@pytest.mark.asyncio
async def test_str_to_json(hub):
    data = '{ "cluster_name": "idem-eks-test", "region": "ap-south-1" }'
    result = await hub.exec.core.conversion.to_json(data=data)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert (
        '"{ \\"cluster_name\\": \\"idem-eks-test\\", \\"region\\": \\"ap-south-1\\" }"'
        == ret["data"]
    )


async def test_dict_to_json(hub):
    # Creating a dictionary
    dict = {
        ("x", "y", "z"): "Elder Wand",
        2: "Expecto",
        3: "Patronum",
        4: "Avada",
        5: "Kedavra",
        6: float("nan"),
    }
    result = await hub.exec.core.conversion.to_json(
        data=dict, skip_keys=True, allow_nan=True, indent=4, separators=(". ", " = ")
    )
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    output = (
        "{\n"
        '    "2" = "Expecto". \n'
        '    "3" = "Patronum". \n'
        '    "4" = "Avada". \n'
        '    "5" = "Kedavra". \n'
        '    "6" = NaN\n'
        "}"
    )
    assert output == ret["data"]


@pytest.mark.asyncio
async def test_to_json_fail_case(hub):
    result = await hub.exec.core.conversion.to_json(data="")
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for json conversion is empty" in result["comment"]

    result = await hub.exec.core.conversion.to_json(data=None)
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for json conversion is empty" in result["comment"]


@pytest.mark.asyncio
async def test_json_to_dict(hub):
    dict_str = '{"test1": "test1_val", "test2": "test2_val"}'
    result = await hub.exec.core.conversion.json_to_dict(data=dict_str)
    assert result["result"], result["ret"]

    dict_data = result["ret"]["data"]

    assert isinstance(dict_data, dict)
    assert "test1" in dict_data
    assert "test2" in dict_data

    assert dict_data["test1"] == "test1_val"
    assert dict_data["test2"] == "test2_val"


@pytest.mark.asyncio
async def test_empty_string_json_to_dict(hub):
    dict_str = ""
    result = await hub.exec.core.conversion.json_to_dict(data=dict_str)
    assert result["result"], result["ret"]
    assert {} == result["ret"]["data"]


@pytest.mark.asyncio
async def test_invalid_syntax_json_to_dict(hub):
    dict_str = '{"test1": "test1_val", {"test2": "test2_val"}'
    result = await hub.exec.core.conversion.json_to_dict(data=dict_str)
    assert not result["result"], not result["ret"]


@pytest.mark.asyncio
async def test_yaml_to_dict(hub):
    dict_str = "names:\n  - name1: test1\n    name2: test2\n"
    result = await hub.exec.core.conversion.yaml_to_dict(data=dict_str)
    assert result["result"], result["ret"]
    dict_data = result["ret"]["data"]

    assert isinstance(dict_data, dict)
    assert "names" in dict_data

    names_data = dict_data["names"][0]
    assert names_data["name1"] == "test1"
    assert names_data["name2"] == "test2"


@pytest.mark.asyncio
async def test_empty_string_yaml_to_dict(hub):
    dict_str = ""
    result = await hub.exec.core.conversion.yaml_to_dict(data=dict_str)
    assert result["result"], result["ret"]
    assert "" == result["ret"]["data"]


@pytest.mark.asyncio
async def test_invalid_syntax_yaml_to_dict(hub):
    dict_str = "names: - name1: test1\n    name2: test2\n"
    result = await hub.exec.core.conversion.yaml_to_dict(data=dict_str)
    assert not result["result"], not result["ret"]
