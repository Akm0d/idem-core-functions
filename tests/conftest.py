import pathlib

import pytest


@pytest.fixture(scope="session")
def code_dir() -> pathlib.Path:
    print(f"code_dir:{pathlib.Path(__file__).parent.parent.absolute()}")
    return pathlib.Path(__file__).parent.parent.absolute()
